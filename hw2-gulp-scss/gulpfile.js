const gulp = require('gulp'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    clean = require('gulp-clean'),
    minify = require('gulp-js-minify'),
    cleanCSS = require('gulp-clean-css'),
    autoprefixer = require('gulp-autoprefixer'),
    imagemin = require('gulp-imagemin'),
    browserSync = require('browser-sync');

const paths = {
    src: {
        styles: "src/scss/**/*.scss",
        script: "src/js/**/*.js",
        img: 'src/img/*.+(png|svg|gif|jpeg|jpg)'
    },
    dist: {
        styles: "dist/css",
        script: "dist/js",
        img: "dist/img",
        dist: "dist/"
    }
}
const cleanDist = () => (
    gulp.src(paths.dist.dist, {allowEmpty: true})
        .pipe(clean())
)
gulp.task('cleanDist', cleanDist);


const buildJS = () => (
    gulp.src(paths.src.script)
        .pipe(concat('script.min.js'))
        .pipe(minify())
        .pipe(gulp.dest(paths.dist.script))
)
gulp.task('buildJS', buildJS);


const buildSCSS = () => (
    gulp.src(paths.src.styles)
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['> 0.5%', 'last 2 versions', 'Firefox ESR', 'Opera 12.1'],
            cascade: false
        }))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(concat('style.min.css'))
        .pipe(gulp.dest(paths.dist.styles))
)
gulp.task('buildSCSS', buildSCSS);


const buildImg = () => (
    gulp.src(paths.src.img)
        .pipe(imagemin())
        .pipe(gulp.dest(paths.dist.img))
)
gulp.task('buildImg',buildImg)


const runDev =() => {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    gulp.watch(paths.src.styles, buildSCSS).on('change', browserSync.reload);
    gulp.watch(paths.src.script, buildJS).on('change', browserSync.reload);
}

gulp.task('build',gulp.series(
    cleanDist,
    buildSCSS,
    buildJS,
    buildImg
))

gulp.task('dev',gulp.series(
    buildSCSS,
    buildJS,
    runDev
))